# crudloop (1844)

> 1844 is the codename for crudloop

> crudloop is an organizations web application

[![Netlify Status](https://api.netlify.com/api/v1/badges/1e4edb27-994d-460c-99ab-8cf160dfc5c4/deploy-status)](https://app.netlify.com/sites/crudloop/deploys)

## Install the dependencies

```bash
npm install
```

### Todo

- Make the site 100% responsive on mobile and desktop ✅
- Fix footer links and make them navigatable ✅
- Make the footer more pleasant ✅
- Add `click to call [tel://]` action on telephone numbers in the footer ✅

* Add head title for individual pages ✅
* Add PWA extension and draw the splashscreens ✅

- Draw and link custom SVG's to third party sites were are using (eg, sailsjs, vue, strapi) ✅
- Make contact form work and send real emails, also add validation to the forms

#### Pages

- Add `about` page with team members
- Add `use case` page - this page contains the kind of applications and things we can build
- Add our social media pages to the footer
- Add our terms of service and privacy policy in pages

## Future

- Add devOps category page

1844&trade; 2019 - 2020&copy; All Rights Reserved
