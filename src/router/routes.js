const routes = [
  {
    path: "/",
    component: () => import("layouts/MyLayout.vue"),
    children: [
      { path: "", component: () => import("pages/Index.vue") },
      { path: "/services", component: () => import("pages/Services.vue") },
      {
        path: "/services/web",
        component: () => import("pages/Services/Web.vue")
      },
      {
        path: "/services/mobile",
        component: () => import("pages/Services/Mobile.vue")
      },
      {
        path: "/services/ui-ux",
        component: () => import("pages/Services/UIUX.vue")
      },
      // RECENT WORKS & PORTFOLIO
      {
        path: "/projects", //-- Oops! threw an error somewhere
        redirect: "/recent"
      },
      {
        path: "/recent",
        component: () => import("pages/RecentWorks/Recent.vue")
      },
      // CONTACT US
      {
        path: "/contact",
        component: () => import("pages/Contact.vue")
      }
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
